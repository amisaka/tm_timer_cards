# stopwatch.py - A program to keep track of speeches timing with colour cards.
# 2020-06-05 - Antonio Misaka - Comments are made by Richard Cook

import webbrowser
import os, sys, time
import argparse, textwrap, random
from tk_timer import StopWatch
from tkinter import *

# Code to allow capture pressed key
if sys.platform[:3] == "win":
    import msvcrt
if sys.platform[:3] == "lin":
    """
    This code captures any pressed key
    """
    import termios, atexit
    from select import select

    # save the terminal settings
    fd = sys.stdin.fileno()
    new_term = termios.tcgetattr(fd)
    old_term = termios.tcgetattr(fd)
    # new terminal setting unbuffered
    new_term[3] = new_term[3] & ~termios.ICANON & ~termios.ECHO

    # switch to normal terminal
    def set_normal_term():
        termios.tcsetattr(fd, termios.TCSAFLUSH, old_term)

    # switch to unbuffered terminal
    def set_curses_term():
        termios.tcsetattr(fd, termios.TCSAFLUSH, new_term)

    def putch(ch):
        sys.stdout.write(ch)

    def getch():
        return sys.stdin.read(1)

    def getche():
        ch = getch()
        putch(ch)
        return ch

    def kbhit():
        dr, dw, de = select([sys.stdin], [], [], 0)
        return dr != []


parser = argparse.ArgumentParser(
    description="""This is an TM timer tool for Timer role.""",
    formatter_class=argparse.RawTextHelpFormatter,
    epilog="""2020 - Antonio Misaka """,
)

parser.add_argument(
    "--argument",
    help=textwrap.dedent(
        """\
    List of standard timing items:
    A - Table Topics, 1 minute
    B - Table Topics, 1:30 minute
    C - Table Topics, 2 minutes
    D - Ice Breaker, 6 minutes
    E - Speech, 7 minutes
    F - Speech, 8 minutes
    G - Speech, 10 minutes
    H - Q&A, 3 minutes
    I - Evaluation, 1 minute
    J - Evaluation, 2 minutes
    K - Evaluation, 2:30 minutes
    L - Workshop, 45 minutes
    M - User defined (you will set Green / Yellow / Red times)
    """
    ),
)
parser.add_argument(
    "-bf", default=False, action="store_false", help="inhibit use of cards"
)

if sys.platform[:3] == "lin":
    parser.add_argument(
        "-wg",
        default=False,
        action="store_false",
        help="Uses a google-chrome webbroser from the system",
    )
    parser.add_argument(
        "-wf",
        default=False,
        action="store_false",
        help="Uses a firefox webbroser from the system",
    )
    parser.add_argument(
        "-wc",
        default=False,
        action="store_false",
        help="Uses a chromium webbroser from the system\nOtherwise, uses a default one.",
    )

if sys.platform[:3] == "win":
    pass
args = parser.parse_args()

wh = 70
ww = 300
lx = 50
ly = 100
timing = list()
lastTime = 0
totalTime = 0
timeFlag = 0
lapNum = 0
dbrowser = session = speaker = browser_path = random_string = ""
kBI = True
flag = True
bFlag = True
g = y = r = n = 0

root = Tk()
root.title("TM's Timer")
root.geometry("%dx%d+%d+%d" % (ww, wh, lx, ly))

startTime = time.time()  # get the first lap's start time


class TMTimer:
    def appConfig():
        """
        This function checks the existence of html files and reconfigures based on passed arguments
        """
        global urlg, urly, urlr, urln, browser, dbrowser, bFlag, browser_path
        flagList = ["green.html", "yellow.html", "red.html", "neutral.html"]
        if sys.platform[:3] == "lin":
            if os.path.exists("/tmp") == True:
                fflag = True
                for n in flagList:
                    if os.path.exists("/tmp/" + n) == False:
                        fflag = False
                if not fflag:
                    print("Creating the files.")
                    os.popen("cp *.html /tmp/.")
                urlg = "file:///tmp/green.html"
                urly = "file:///tmp/yellow.html"
                urlr = "file:///tmp/red.html"
                urln = "file:///tmp/neutral.html"
            else:
                os.mkdir("~/tmp")
                for n in flagList:
                    os.system(f"cp {n} ~/tmp/")
                tuser = os.popen("echo $USER").read().rstrip()
                urlg = "file:///home/" + tuser + "/tmp/green.html"
                urly = "file:///home/" + tuser + "/tmp/yellow.html"
                urlr = "file:///home/" + tuser + "/tmp/red.html"
                urln = "file:///home/" + tuser + "/tmp/neutral.html"
        if sys.platform[:3] == "win":
            browser_path = (
                "C:\\Program Files\ (x86)/Internet\ Explorer\\iexplore.exe %s"
            )
            if os.path.exists("c:\\tmp") == False:
                os.mkdir("c:\\tmp")
            else:
                if os.path.exists("c:\\tmp\neutral.html") == False:
                    os.system("copy *.html c:\\tmp")
                urlg = "c:/tmp/green.html"
                urly = "c:/tmp/yellow.html"
                urlr = "c:/tmp/red.html"
                urln = "c:/tmp/neutral.html"
        try:
            for arg in sys.argv:
                if arg == "-bf":
                    bFlag = False
                if sys.platform[:3] == "lin":
                    if arg == "-wc":
                        browser = webbrowser.get("chromium")
                        dbrowser = "chromium"
                    elif arg == "-wf":
                        browser = webbrowser.get("firefox")
                        dbrowser = "firefox"
                    elif arg == "-wg":
                        browser = webbrowser.get(
                            "google-chrome"
                        )  # fireflox, google-chrome, chromium
                        dbrowser = "chrome"
                    else:
                        browser = webbrowser.get()
                        dbrowser = browser.name
                if sys.platform[:3] == "win":
                    browser = webbrowser.get("windows-default")
                    dbrowser = "iexplore.exe"
            # print("Value wbrowser: ",type(browser))
        except IndexError:
            pass

    def timerLoop():
        """
        this function keeps track of the timing task
        """
        global lapTime, totalTime, lastTime, startTime, sseconds, sminutes, tseconds, tminutes, thours
        lapTime = round(time.time() - lastTime, 1)
        totalTime = round(time.time() - startTime, 1)
        sseconds = lapTime
        sminutes = sseconds // 60
        tseconds = totalTime
        tminutes = tseconds // 60
        thours = tminutes // 60

    def timingConv(tmg):
        """
        Converting the values to their respective colour cards
        """
        global gs, ys, rs, gc, yc, rc
        timing = tmg
        gs = timing[0].split(":")
        ys = timing[1].split(":")
        rs = timing[2].split(":")
        if len(gs) == 2:
            gc = int(gs[0]) * 60 + int(gs[1])
        else:
            gc = int(gs[0]) * 60
        if len(ys) == 2:
            yc = int(ys[0]) * 60 + int(ys[1])
        else:
            yc = int(ys[0]) * 60
        if len(rs) == 2:
            rc = int(rs[0]) * 60 + int(rs[1])
        else:
            rc = int(rs[0]) * 60

    def showFlag(tF):
        """
        This function controls the displaying of Neutral, Green, Yellow or Red cards
        """
        global flag, lastTime, r, g, y, n, browser, fd, new_term, old_term
        if sys.platform[:3] == "lin":
            atexit.register(set_normal_term)
            set_curses_term()
        timeFlag = tF
        while flag:
            timeFlag = round(time.time() - lastTime, 1)
            if timeFlag >= rc:
                if r == 0:
                    browser.open(urlr, new=2)
                    r = 1
                    flag = False
                if sys.platform[:3] == "lin":
                    set_normal_term()
            elif timeFlag >= yc:
                if y == 0:
                    browser.open(urly, new=2)
                    y = 1
            elif timeFlag >= gc:
                if g == 0:
                    browser.open(urlg, new=2)
                    g = 1
            else:
                if n == 0:
                    browser.open(urln, new=2)
                    n = 1

            if sys.platform[:3] == "win":
                if msvcrt.kbhit():
                    if ord(msvcrt.getch()) != None:
                        kBI = flag = False
            if sys.platform[:3] == "lin":
                if kbhit():
                    ch = getch()
                    if ch != None:
                        kBI = flag = False
                        set_normal_term()

    def gen_string():
        global random_string, session
        for _ in range(6):
            # Considering only upper and lowercase letters
            random_integer = random.randint(97, 97 + 26 - 1)
            flip_bit = random.randint(0, 1)
            # Convert to lowercase if the flip bit is on
            random_integer = random_integer - 32 if flip_bit == 1 else random_integer
            # Keep appending random characters using chr(x)
            random_string += chr(random_integer)
        session = random_string
        print(
            f"*** Because session name is None, a random name is generated: {session}"
        )

    def saveNotes(s_name):
        """
        This function saves the speaker time in a filename from its session name
        If the application is restarted, use the same session name to allow recording
        new speaker time.
        """
        lsession = s_name
        global sminutes, sseconds
        if lsession == "":
            TMTimer.gen_string()
            # print(session)
        wsession = session + ".txt"
        with open(wsession, "a") as timer_role:
            timer_role.write(speaker + " - ")
            timer_role.write("time: %02d:%02d" % (sminutes, sseconds % 60) + "\n")

    def checkTiming():
        """
        This function accepts the timing and check the input of values
        """
        global timing
        try:
            timing = list(
                input(
                    "Timing (GRN, YLW, RED)\ni.e. an M value for just RED or\n M or M:SS with commas for each GRN, YLW and RED\nM = integer; SS = two digit integer : "
                )
                .strip()
                .split(",")
            )
        except KeyboardInterrupt:
            sys.exit(">>> Timing application cancelled by the user.")
        sameValue = set(timing)
        if len(sameValue) == 1:
            try:
                TMTimer.expandTiming()
                return
            except ValueError:
                excT = "".join(map(str, timing))  # Exception for 1:30 and 2:30
                excV = int(excT[0])
                if ":" in excT and excV <= 2:
                    TMTimer.treatTiming()
                    return
                else:
                    print("This value can not be accepted! - ", timing)
                print("Must be an integer!\nExceptions are: 1:30 or 2:30")
                TMTimer.checkTiming()
        elif len(sameValue) != 3:
            print("Please entry distinct values!")
            TMTimer.checkTiming()
        else:
            for n in range(len(timing)):
                try:
                    tmp = timing[n].split(":")
                    if len(tmp) == 2:
                        v1 = int(tmp[0])
                        v2 = int(tmp[1])
                    else:
                        v1 = int(tmp[0])
                except ValueError:
                    print(timing)
                    print(
                        "Value Error: Missing a value number alone or between ':', please review it."
                    )
                    TMTimer.checkTiming()
            if len(timing) != 3:
                print(timing)
                print(
                    "Data Entry Error: Please enter one value for each GRN,YLW,RED separated by commas.\nOr an integer value."
                )
                TMTimer.checkTiming()
            else:
                sortedTiming = sorted(timing)
                if sortedTiming != timing:
                    print(
                        "Ascending Input Error: The values must be in ascending order! Please review it."
                    )
                    TMTimer.checkTiming()

    def expandTiming():
        """
        This function is treating alone integer values input
        """
        try:
            global timing
            tmp = timing[:]
            m = int(tmp[0])
            if int(timing[0]) == 1:
                timing[0] = "0:30"
                timing.append("0:45")
                timing.append(tmp[0])
                t = timing[:]
            elif int(timing[0]) < 5:
                t = [None] * 3
                for i in range(3):
                    if i == 0:
                        t[i] = str(m - 1)
                    elif i == 1:
                        t[i] = str(m - 1) + ":30"
                    else:
                        t[i] = timing[0]
                timing = t[:]
            else:
                t = [None] * 3
                for i in range(3):
                    if i == 0:
                        t[i] = str(m - 2)
                    elif i == 1:
                        t[i] = str(m - 1)
                    else:
                        t[i] = tmp[0]
            timing = t[:]
        except ValueError:
            TMTimer.checkTiming()

    def treatTiming():
        """
        This function is treating exception for 1:30 and 2:30 inputs
        """
        try:
            global timing
            vtiming = "".join(map(str, timing))
            tmp = [None] * 3
            vt = vtiming.split(":")
            ivt1 = int(vt[0])
            ivt2 = int(vt[1])
            if ivt1 == 1:
                for i in range(3):
                    if i == 0:
                        tmp[i] = str(ivt1)
                    elif i == 1:
                        tmp[i] = str(ivt1) + ":" + str(ivt2 // 2)
                    else:
                        tmp[i] = timing[0]
            elif ivt1 == 2:
                for i in range(3):
                    if i == 0:
                        tmp[i] = str(ivt1 - 1) + ":" + str(ivt2)
                    elif i == 1:
                        tmp[i] = str(ivt1)
                    else:
                        tmp[i] = timing[0]
            else:
                TMTimer.checkTiming()
            timing = tmp[:]
        except ValueError:
            TMTimer.checkTiming()

    def main():
        global bFlag, dbrowser, browser, kBI, flag, lapNum, timing, totalTime, lastTime, timeFlag, speaker, session, g, y, r, n
        TMTimer.appConfig()
        # Display the program's instructions.
        print("Enter data as prompted. To exit / quit, leave speaker's name empty.")
        try:
            session = input("Enter a file name for the timing log: ")
        except KeyboardInterrupt:
            sys.exit(">>> Application stopped by the user.")
        if session == "":
            TMTimer.gen_string()
        lapNum = 1
        print("\n===========================================")
        speaker = input("Speaker's name: ")  # press Enter to begin
        if speaker == "":
            sys.exit(
                ">>> Application has been finished by the user with void Speaker's name."
            )
        TMTimer.checkTiming()
        TMTimer.timingConv(timing)
        print(
            f"The cards are: GRN at {timing[0]}, YLW at {timing[1]}, RED at {timing[2]}"
        )
        print(
            "Press any key if speaker finishes before the Red Card\nThen press Enter."
        )
        print("\n--------------------------------------------")
        input("Press Enter when speaker %s starts" % speaker)
        w = StopWatch(root)
        w.Reset()
        w.pack(side=TOP)
        w.Start()
        lastTime = time.time()

        # Start tracking the lap times.
        try:
            while True:
                timeFlag = round(time.time() - lastTime, 1)
                g = y = r = n = 0
                flag = True
                # print("Value bFlag: ",bFlag)
                if bFlag:
                    TMTimer.showFlag(timeFlag)

                if kBI:
                    input("Press Enter when speaker %s stops." % speaker)
                TMTimer.timerLoop()
                w.Stop()
                kBI = True
                # if sys.platform[:3] == 'lin':
                #     os.system(f"killall -9 '{dbrowser}'")
                # elif sys.platform[:3] == 'win':
                #     os.system(f"TASKKILL /IM {dbrowser}")
                print(
                    "\n#%s: Speaker: %s time: %02d:%02d ( Session time %01d:%02d:%02d )"
                    % (
                        lapNum,
                        speaker,
                        sminutes,
                        sseconds % 60,
                        thours,
                        tminutes % 60,
                        tseconds % 60,
                    ),
                    end="",
                )
                print("\nPlease delete the timing cards before you continue.")
                TMTimer.saveNotes(session)
                lapNum += 1
                print("\n===========================================")
                w.Reset()
                speaker = input(
                    "Speaker's name: "
                )  # Type the name and press Enter to begin
                if speaker == "":
                    raise KeyboardInterrupt
                TMTimer.checkTiming()
                TMTimer.timingConv(timing)
                print(
                    f"The cards are: GRN at {timing[0]}, YLW at {timing[1]}, RED at {timing[2]}"
                )
                print(
                    "Press any key if speaker finishes before the Red Card\nThen press Enter."
                )
                print("\n--------------------------------------------")
                input("Press Enter when speaker %s starts" % speaker)
                w.Start()
                lastTime = time.time()  # reset the last laptime

        except KeyboardInterrupt:
            # Handle the Crtl-C exception to keep its error message from displaying.
            seconds = totalTime
            minutes = seconds // 60
            hours = minutes // 60
            print(
                "\n*** Total time of the session: %02d:%02d:%02d ***"
                % (hours, minutes % 60, seconds % 60)
            )
            print("\nTiming log has been written to: " + session + ".txt")


if __name__ == "__main__":
    TMTimer.main()
